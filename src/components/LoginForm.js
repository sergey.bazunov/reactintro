import React, { useEffect, useState } from 'react';
import { FormGroup, Input, Label, Button } from 'reactstrap';
import axios from 'axios';
import ServerResponse from './ServerResponse';

function LoginForm() {

    const [email, setEmail] = useState('');
    const [passw, setPassw] = useState('');
    const [resultMessage, setResultMessage] = useState('');
    const [resultColor, setResultColor] = useState('secondary');

    function handleSubmit(e) {
        
        e.preventDefault();   

        axios.post("http://localhost:5000", {email: email, passw: passw} )
            .then(res => {
                console.log(res);
                setResultMessage(res.data);
                setResultColor('success')
            })
            .catch(error => {
                // {message, name, code, config, request, response}                                
                setResultMessage(error.message);                
                setResultColor('danger')
            });          
    }
    
    function emailChange(event) {
        setEmail(event.target.value);
    }    

    function passwdChange(event) {
        setPassw(event.target.value);
    }    

    return (
        <div className="container mt-4 p-2 border rounded">
            <div className="row">
                <div className='col'></div>
                <form className='col-8' onSubmit={handleSubmit}>
                    <FormGroup size="lg">
                        <Label for="Email">Email</Label>
                        <Input id="Email" name="email" placeholder="Enter email" type="email" required onChange={emailChange}/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="Password">Password</Label>
                        <Input id="Password" name="password" placeholder="Enter password" type="password" required onChange={passwdChange} />    
                    </FormGroup>
                    <Button className='mb-2'>Submit</Button>
                    <ServerResponse data={resultMessage} color={resultColor} />
                </form>
                <div className='col'></div>                
            </div>
        </div>
    );
}

export default LoginForm
