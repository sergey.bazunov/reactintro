import React from 'react'
import { Alert } from 'reactstrap';

function ServerResponse(props) {
    return (
        (props.data ?
            <Alert color={props.color}>
                {props.data}
            </Alert>
            : null )
    );
}

export default ServerResponse;